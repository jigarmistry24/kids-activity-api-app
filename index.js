const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const multer = require('multer');

var routes = require('./routes/routes');

const app = express();

app.use(bodyParser.json());

app.use('/data', express.static(path.join(__dirname, 'data')))

app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/api', routes);

const storageProducts = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './data/images/');
    },
    filename(req, file, cb) {
        console.log(req.params);
        cb(null, `${new Date().getTime()}-${file.originalname}`);
    },
});

const uploadActivity = multer({
    storage: storageProducts
});

app.post('/api/uploads', uploadActivity.single('file'), (req, res) => {
    const file = req.file;
    if (file.path) {
        res.json({
            "status": 1,
            "image_path": file.path
        });
    } else {
        res.json({
            "status": 0,
            "message": "Unable to upload the image. Please try again"
        });
    }
});


module.exports = app;