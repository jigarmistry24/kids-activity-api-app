var mongoose = require('../db/dbConnect');
var Schema = mongoose.Schema;

var mobUserSchema = new Schema({
    name: String,
    password: String,
    country: String,
    mobile_number: String,
    city: String,
    email: String,
    child: Object
});

var MobileUser = mongoose.model('MobileUser', mobUserSchema, 'MobileUsers');

module.exports = MobileUser;