var mongoose = require('../db/dbConnect');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String,
    password: String,
    type: String,
    email: String,
    academy: mongoose.Schema.Types.ObjectId,
    permissions: Array
});

var SiteUser = mongoose.model('SiteUser', userSchema, 'SiteUsers');

module.exports = SiteUser;