var mongoose = require('../db/dbConnect');
var Schema = mongoose.Schema;

var activitySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    academy: Object, // {ObjectId, Name}
    category: String,
    country: String,
    gender: String,
    city: String,
    detail: String,
    age: String,
    image_path: Array,
    activity_date: String,
    activity_time: String,
    price: String,
    is_home: String,
    video_link: String
});

var Activity = mongoose.model('Activity', activitySchema, 'Activity');

module.exports = Activity;