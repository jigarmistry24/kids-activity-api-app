var mongoose = require('../db/dbConnect');
var Schema = mongoose.Schema;

var blogSchema = new Schema({
    title: String,
    slug: {
        type: String,
        required: true,
        unique: true
    },
    description: String,
    detail: String,
    image_path: String,
    created_date: Date
});

var Blog = mongoose.model('Blog', blogSchema, 'Blogs');

module.exports = Blog;