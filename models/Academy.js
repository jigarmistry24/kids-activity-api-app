var mongoose = require('../db/dbConnect');
var Schema = mongoose.Schema;

var academySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    country: String,
    city: String,
    phone_number: String,
    website: String,
    image: String,
    email: String,
    twitter: String,
    instagram: String,
    linkedin: String    
});

var Academy = mongoose.model('Academy', academySchema, 'Academy');

module.exports = Academy;