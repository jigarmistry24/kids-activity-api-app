var crypto = require('crypto');
var https = require('https');
var bcrypt = require('bcrypt');
var dbHandler = require('../db/mobHandler');

module.exports.addMobileUser = function (req, res) {
    let name = req.body.name;
    let country = req.body.country;
    let city = req.body.city;
    let mobile_number = req.body.mobile_number;
    let password = req.body.password;
    let email = req.body.email;
    let child = req.body.child;
    // child
    // name, gender, age

    if (password && password != "")
        password = bcrypt.hashSync(password, 8);

    let user_data = {
        name: name,
        country: country,
        mobile_number: mobile_number,
        city: city,
        password: password,
        email: email,
        child: child
    };

    dbHandler.addMobileUser(user_data, function (data) {
        res.json(data);
    });
}

module.exports.getMobileUsers = function (req, res) {
    dbHandler.getMobileUsers(function (data) {
        res.json(data);
    });
}

module.exports.login = function (req, res) {
    let email = req.body.email;
    let password = req.body.password;
    dbHandler.getMobileUserByEmail(email, function (data) {
        if (data['status']) {
            if (data['data'][0]) {
                if (bcrypt.compareSync(password, data['data'][0].password)) {
                    res.json({
                        "status": 1,
                        "message": "Login Successfully",
                        "data": {
                            id: data['data'][0]['_id'],
                            name: data['data'][0]['name'],
                        }
                    })
                } else {
                    res.json({
                        status: 0,
                        message: "Password is not correct"
                    });
                }
            } else {
                res.json({
                    status: 0,
                    message: "User does not exist"
                });
            }
        } else {
            res.json(data);
        }
    });
}