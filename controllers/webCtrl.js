var crypto = require('crypto');
var https = require('https');
var dbHandler = require('../db/webHandler');

module.exports.addAcademy = function (req, res) {
    let name = req.body.name;
    let country = req.body.country;
    let city = req.body.city;
    let phone_number = req.body.phone_number;
    let website = req.body.website;
    let image = req.body.image;
    let email = req.body.email;
    let twitter = req.body.twitter;
    let instagram = req.body.instagram;
    let linkedin = req.body.linkedin;

    let academy_data = {
        name: name,
        country: country,
        email: email,
        city: city,
        phone_number: phone_number,
        website: website,
        image: image,
        twitter: twitter,
        instagram: instagram,
        linkedin: linkedin
    };

    dbHandler.addAcademy(academy_data, function (data) {
        res.json(data);
    });
}

module.exports.getAcademies = function (req, res) {
    dbHandler.getAcademies(function (data) {
        res.json(data);
    });
}

module.exports.getAcademyById = function (req, res) {
    let academy_id = req.params.academy_id;
    let data = {
        academy_id: academy_id
    }
    dbHandler.getAcademyById(data, function (data) {
        res.json(data);
    });
}

module.exports.updateAcademy = function (req, res) {
    let id = req.body.id;
    let name = req.body.name;
    let country = req.body.country;
    let email = req.body.email;
    let city = req.body.city;
    let phone_number = req.body.phone_number;
    let website = req.body.website;
    let image = req.body.image;
    let twitter = req.body.twitter;
    let instagram = req.body.instagram;
    let linkedin = req.body.linkedin;

    let academy_data = {
        name: name,
        country: country,
        city: city,
        email: email,
        phone_number: phone_number,
        website: website,
        image: image,
        twitter: twitter,
        instagram: instagram,
        linkedin: linkedin
    };

    dbHandler.updateAcademy(id, academy_data, function (data) {
        res.json(data);
    });
}

module.exports.deleteAcademy = function (req, res) {
    let id = req.params.academy_id;
    dbHandler.deleteAcademyById(id, function (data) {
        res.json(data);
    });
}

module.exports.addBlog = function (req, res) {
    let title = req.body.title;
    let slug = title.toLowerCase().replace(/ /g, "-");
    let detail = req.body.detail;
    let description = req.body.description;
    let image_path = req.body.image_path;
    let created_date = new Date();

    let blog_data = {
        title: title,
        slug: slug,
        detail: detail,
        description: description,
        image_path: image_path,
        created_date: created_date
    };

    dbHandler.addBlog(blog_data, function (data) {
        res.json(data);
    });
}

module.exports.getBlogs = function (req, res) {
    dbHandler.getBlogs(function (data) {
        res.json(data);
    });
}

module.exports.getBlogById = function (req, res) {
    let blog_id = req.params.blog_id;
    let data = {
        blog_id: blog_id
    }
    dbHandler.getBlogById(data, function (data) {
        res.json(data);
    });
}

module.exports.updateBlog = function (req, res) {
    let id = req.body.id;
    let title = req.body.title;
    let description = req.body.description;
    let slug = title.toLowerCase().replace(/ /g, "-");
    let detail = req.body.detail;
    let image_path = req.body.image_path;

    let blog_data = {
        title: title,
        slug: slug,
        description: description,
        detail: detail,
        image_path: image_path
    };

    dbHandler.updateBlog(id, blog_data, function (data) {
        res.json(data);
    });
}

module.exports.deleteBlog = function (req, res) {
    let id = req.params.blog_id;
    dbHandler.deleteBlogById(id, function (data) {
        res.json(data);
    });
}

module.exports.addActivity = function (req, res) {
    let name = req.body.name;
    let country = req.body.country;
    let city = req.body.city;
    let academy = req.body.academy;
    let gender = req.body.gender;
    let category = req.body.category;
    let detail = req.body.detail;
    let age = req.body.age;
    let image_path = req.body.image_path;
    let activity_date = req.body.activity_date;
    let activity_time = req.body.activity_time;
    let price = req.body.price;
    let is_home = req.body.is_home;
    let video_link = req.body.video_link;

    let activity_data = {
        name: name,
        country: country,
        gender: gender,
        city: city,
        category: category,
        academy: academy,
        detail: detail,
        age: age,
        image_path: image_path,
        activity_date: activity_date,
        activity_time: activity_time,
        price: price,
        is_home: is_home,
        video_link: video_link
    };

    dbHandler.addActivity(activity_data, function (data) {
        res.json(data);
    });
}

module.exports.getActivities = function (req, res) {
    dbHandler.getActivities(function (data) {
        res.json(data);
    });
}

module.exports.getActivityById = function (req, res) {
    let activity_id = req.params.activity_id;
    let data = {
        activity_id: activity_id
    }
    dbHandler.getActivityById(data, function (data) {
        res.json(data);
    });
}

module.exports.getActivitiesByAcademyId = function (req, res) {
    let academy_id = req.params.academy_id;
    dbHandler.getActivitiesByAcademyId(academy_id, function (data) {
        res.json(data);
    });
}

module.exports.updateActivity = function (req, res) {
    let id = req.body.id;
    let name = req.body.name;
    let country = req.body.country;
    let city = req.body.city;
    let gender = req.body.gender;
    let category = req.body.category;
    let academy = req.body.academy;
    let detail = req.body.detail;
    let age = req.body.age;
    let image_path = req.body.image_path;
    let activity_date = req.body.activity_date;
    let activity_time = req.body.activity_time;
    let price = req.body.price;
    let is_home = req.body.is_home;
    let video_link = req.body.video_link;

    let activity_data = {
        name: name,
        country: country,
        gender: gender,
        city: city,
        academy: academy,
        category: category,
        detail: detail,
        age: age,
        image_path: image_path,
        activity_date: activity_date,
        activity_time: activity_time,
        price: price,
        is_home: is_home,
        video_link: video_link
    };

    dbHandler.updateActivity(id, activity_data, function (data) {
        res.json(data);
    });
}

module.exports.getActivityByIds = function (req, res) {
    let activity_ids = req.body.activity_ids;
    let data = {
        activity_ids: activity_ids
    }
    dbHandler.getActivityByIds(data, function (data) {
        res.json(data);
    });
}

module.exports.deleteActivity = function (req, res) {
    let id = req.params.activity_id;
    dbHandler.deleteActivityById(id, function (data) {
        res.json(data);
    });
}