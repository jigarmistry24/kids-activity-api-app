
Siteuser:
- Name
- Email
- Password
- Type
- Academy(Reference)

Academy:
- Image
- Name
- Location(Country, City)
- Phone Number
- Website
- Email
- Twitter
- Instagram
- Linkedin

Activity
- Image
- Name
- Location
- Location(Country, City)
- Academy (Reference)
- Detail
- Age(Range)
- Gender
- Date
- Time
- Price
- isHome
- videoLink

Blog
- Title
- Slug
- Detail
- Date

Register
- Email
- Phone Number
- Country
- City
- Password
- Child 
    - Name
    - Gender
    - Age