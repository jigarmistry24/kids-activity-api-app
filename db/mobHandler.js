var MobileUser = require('../models/MobileUser');

var ObjectId = require('mongodb').ObjectId;
var fs = require('fs');

module.exports.addMobileUser = function (user_data, cb) {
    var newUser = new MobileUser(user_data);
    newUser.save(function (err) {
        if (err && err.code == 11000) {
            return cb({
                status: 0,
                message: "User Already Exits"
            });
        }
        cb({
            status: 1,
            message: 'User Added'
        });
    });
}

module.exports.getMobileUsers = function (cb) {
    MobileUser.find({}, function (err, users) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: users
        });
    });
}


module.exports.getMobileUserByEmail = function (email, cb) {
    MobileUser.find({
        email: email
    }, function (err, user) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: user
        });
    });
}