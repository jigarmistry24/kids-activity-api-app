var SiteUser = require('../models/SiteUser');
var Academy = require('../models/Academy');
var Activity = require('../models/Activity');
var Blog = require('../models/Blog');

var ObjectId = require('mongodb').ObjectId;
var fs = require('fs');

module.exports.addAcademy = function (academy_data, cb) {
    var newAcdamey = new Academy(academy_data);
    newAcdamey.save(function (err) {
        if (err && err.code == 11000) {
            return cb({
                status: 0,
                message: "Academy Already Exits"
            });
        }
        cb({
            status: 1,
            message: 'Academy Added'
        });
    });
}

module.exports.getAcademies = function (cb) {
    Academy.find({}, function (err, academies) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: academies
        });
    });
}

module.exports.updateAcademy = function (id, data, cb) {
    Academy.findById(new ObjectId(id), function (err, academy) {
        if (err) throw err;

        academy.name = data.name;
        academy.country = data.country;
        academy.city = data.city;
        academy.phone_number = data.phone_number;
        academy.website = data.website;
        academy.image = data.image;
        academy.twitter = data.twitter;
        academy.instagram = data.instagram;
        academy.linkedin = data.linkedin;

        academy.save(function (err) {
            if (err) {
                return cb({
                    status: 0,
                    message: "Something went wrong"
                });
            }
            cb({
                status: 1
            });
        });
    });
}

module.exports.getAcademyById = function (data, cb) {
    Academy.findById(new ObjectId(data['academy_id']), function (err, academy) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        }
        cb({
            status: 1,
            data: academy
        });
    });
}

module.exports.deleteAcademyById = function (id, cb) {
    Academy.findByIdAndRemove(new ObjectId(id), function (err) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            message: "Academy Removed From System !"
        });
    });
}

module.exports.addBlog = function (blog_data, cb) {
    var newBlog = new Blog(blog_data);
    newBlog.save(function (err) {
        if (err && err.code == 11000) {
            return cb({
                status: 0,
                message: "Blog Already Exits"
            });
        }
        cb({
            status: 1,
            message: 'Blog Added'
        });
    });
}

module.exports.getBlogs = function (cb) {
    Blog.find({}, function (err, blogs) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: blogs
        });
    });
}

module.exports.getBlogById = function (data, cb) {
    Blog.findById(new ObjectId(data['blog_id']), function (err, blog) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        }
        cb({
            status: 1,
            data: blog
        });
    });
}

module.exports.updateBlog = function (id, data, cb) {
    Blog.findById(new ObjectId(id), function (err, blog) {
        if (err) throw err;

        blog.title = data.title;
        blog.slug = data.slug;
        blog.detail = data.detail;
        blog.image_path = data.image_path;

        blog.save(function (err) {
            if (err) {
                return cb({
                    status: 0,
                    message: "Something went wrong"
                });
            }
            cb({
                status: 1
            });
        });
    });
}

module.exports.deleteBlogById = function (id, cb) {
    Blog.findByIdAndRemove(new ObjectId(id), function (err) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            message: "Blog Removed From System !"
        });
    });
}

module.exports.addActivity = function (activity_data, cb) {
    var newActivity = new Activity(activity_data);
    newActivity.save(function (err) {
        if (err && err.code == 11000) {
            return cb({
                status: 0,
                message: "Activity Already Exits"
            });
        }
        cb({
            status: 1,
            message: 'Activity Added'
        });
    });
}

module.exports.getActivities = function (cb) {
    Activity.find({}, function (err, activities) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: activities
        });
    });
}

module.exports.getActivityById = function (data, cb) {
    Activity.findById(new ObjectId(data['activity_id']), function (err, activity) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        }
        cb({
            status: 1,
            data: activity
        });
    });
}

module.exports.getActivitiesByAcademyId = function (academy_id, cb) {
    Activity.find({
        "academy.id": academy_id
    }, function (err, activities) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: activities
        });
    });
}

module.exports.updateActivity = function (id, data, cb) {
    Activity.findById(new ObjectId(id), function (err, activity) {
        if (err) throw err;

        activity.name = data.name;
        activity.country = data.country;
        activity.city = data.city;
        activity.category = data.category;
        activity.academy = data.academy;
        activity.detail = data.detail;
        activity.age = data.age;
        activity.image_path = data.image_path;
        activity.activity_date = data.activity_date;
        activity.activity_time = data.activity_time;
        activity.price = data.price;
        activity.is_home = data.is_home;
        activity.video_link = data.video_link;

        activity.save(function (err) {
            if (err) {
                return cb({
                    status: 0,
                    message: "Something went wrong"
                });
            }
            cb({
                status: 1
            });
        });
    });
}

module.exports.getActivityByIds = function (data, cb) {
    let activites_ids = [];
    let activity_data = data['activity_ids'];
    for (var index = 0; index < activity_data.length; index++) {
        activites_ids.push(new ObjectId(activity_data[index]));
    }
    Activity.find({
        '_id': {
            $in: activites_ids
        }
    }, function (err, activities) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            data: activities
        });
    });
}

module.exports.deleteActivityById = function (id, cb) {
    Activity.findByIdAndRemove(new ObjectId(id), function (err) {
        if (err) {
            return cb({
                status: 0,
                message: "Something went wrong"
            });
        };
        cb({
            status: 1,
            message: "Activity Removed From System !"
        });
    });
}