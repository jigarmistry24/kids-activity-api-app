var express = require('express');
var api = express.Router();

var webCtrl = require('../controllers/webCtrl');
var mobCtrl = require('../controllers/mobCtrl');

api.get('/', function (req, res) {
    let hours = new Date().getHours();
    let api_data = {
        'name': 'API Routes',
        'time': {
            hours: hours
        }
    };
    res.send(api_data);
});

api.use(function checkApiToken(req, res, next) {
    if (req.headers.apitoken || req.query.apitoken) {
        next()
    } else {
        res.json({
            status: 0,
            message: 'Invalid or expired token'
        });
    }
});

api.get('/academy', webCtrl.getAcademies);
api.get('/academy/:academy_id', webCtrl.getAcademyById);
api.post('/academy', webCtrl.addAcademy);
api.patch('/academy', webCtrl.updateAcademy);
api.delete('/academy/:academy_id', webCtrl.deleteAcademy);

api.get('/activity', webCtrl.getActivities);
api.get('/activity/:activity_id', webCtrl.getActivityById);
api.get('/activity/academy/:academy_id', webCtrl.getActivitiesByAcademyId);
api.post('/activity', webCtrl.addActivity);
api.post('/activity/filter', webCtrl.getActivityByIds);
api.patch('/activity', webCtrl.updateActivity);
api.delete('/activity/:activity_id', webCtrl.deleteActivity);

api.get('/user/mobile', mobCtrl.getMobileUsers);
api.post('/user/mobile', mobCtrl.addMobileUser);
api.post('/user/mobile/login', mobCtrl.login);

api.get('/blog', webCtrl.getBlogs);
api.get('/blog/:blog_id', webCtrl.getBlogById);
api.post('/blog', webCtrl.addBlog);
api.patch('/blog', webCtrl.updateBlog);
api.delete('/blog/:blog_id', webCtrl.deleteBlog);

module.exports = api